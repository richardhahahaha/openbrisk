/*
 * Copyright 2013 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __BRISK_IMAGE_H__
#define __BRISK_IMAGE_H__

#include <assert.h>

#include <BRISK/include/Compiler.h>
#include <BRISK/include/BriskTypes.h>
#include <BRISK/include/BriskMemory.h>

/* This BRISK implementation expects an 8-bit grayscale image input only. */

/* On embedded platforms (e.g. ADI DSP), the frame data captured from an video
 * device would be copied to the struct BriskImage through DMA.
 *
 * This file would be modified to fit a specific application, the following 
 * fields are essential:
 *
 *     =================================================
 *     | pixel   | the frame data buffer               |
 *     |---------+-------------------------------------|
 *     | width   | the width / height of the frame     |
 *     | height  |                                     |
 *     |---------+-------------------------------------|
 *     | format  | (optional)                          |
 *     |         | equals constant 2 (grayscale image) |
 *     =================================================
 *
 * --------------------------------------------------------------------------- *
 *                                                                             *
 *      scan direction                                                         *
 *     --------------->   ==================================================   *
 *     x x x x x x x x |  | pixel  | address of the first pixel (top left) |   *
 *     x x x x x x x x |  |--------+---------------------------------------|   *
 *     x x x x x x x x |  | width  | 8                                     |   *
 *     x x x x x x x x |  |--------+---------------------------------------|   *
 *     x x x x x x x x |  | height | 7                                     |   *
 *     x x x x x x x x |  |--------+---------------------------------------|   *
 *     x x x x x x x x |  | format | 2 (optional)                          |   *
 *                     *  ==================================================   *
 *     (x = 8-bit unsigned grayscale pixel)                                    *
 *                                                                             *
 * --------------------------------------------------------------------------- *
 */

#define BRISK_IMAGE_FORMAT_GRAY    2
#define BRISK_IMAGE_FORMAT_OTHER  -1

typedef struct
{
    b_uint8_t *pixel;
    b_int_t width;
    b_int_t height;
    
    b_int_t format;
} BriskImage;

/* Creates and copy a new grayscale BriskImage from a given pointer
 * to a BriskImage and returns the pointer to the new image. */
INLINE BriskImage *briskImageGrayCopy(const BriskImage *image);

/* Initializes the given image to be an
 * image with given width, height and format. */
INLINE BriskImage *briskImageInitWithSize(BriskImage *image,
                                          const b_uint32_t width,
                                          const b_uint32_t height,
                                          const b_int_t format);

/* Creates an image an initialzes it. */
INLINE BriskImage *briskImageAllocWithSize(const b_uint32_t width,
                                           const b_uint32_t height,
                                           b_int_t format);

/* Frees the memory of the given image. */
INLINE void briskImageRelease(BriskImage *image);

#endif /* __BRISK_IMAGE_H__ */
