/*
 * Copyright 2013 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <BRISK/include/BriskPrototypes.h>

static INLINE b_uint8_t _grayGetPixel(const BriskImage *image,
                                      b_int_t x, b_int_t y)
{
    b_int_t i = x, j = y;
    
    /* boundaries */
    if(i < 0) i = 0; if(j < 0) j = 0;
    if(i > image->width) i = image->width;
    if(j > image->height) j = image->height;
    
    return image->pixel[j * image->width + i];
}

static INLINE void _graySetPixel(BriskImage *image,
                                 b_int_t x, b_int_t y, b_uint8_t value)
{
    image->pixel[y * image->width + x] = value;
}

BriskImage *briskLayerHalfSample(const BriskImage *source,
                                 BriskImage *destination)
{
    /* 1/2 resampling, thus scale = 2 */
    const b_float_t scale = 2.0f;
    b_int_t i, j;
    
    assert(source->width / 2 == destination->width);
    assert(source->height / 2 == destination->height);
    
    for(i = 0; i < destination->width; i++)
    {
        for(j = 0; j < destination->height; j++)
        {
            b_int_t x = i * scale;
            b_int_t y = j * scale;
            
            b_float_t u = 0.1f; /*(float)i * scale - x; */
            b_float_t v = 0.1f; /*(float)j * scale - y; */
            
            b_uint8_t r1 = _grayGetPixel(source, x, y);
            b_uint8_t r2 = _grayGetPixel(source, x, y + 1);
            b_uint8_t r3 = _grayGetPixel(source, x + 1, y);
            b_uint8_t r4 = _grayGetPixel(source, x + 1, y + 1);
            
            b_uint8_t r = (b_uint8_t)((1.0f - u) * (1.0f - v) * r1 + (1.0f - u) * v
                                  * r2 + u * (1.0f - v) * r3 + u * v * r4);
            
            _graySetPixel(destination, i, j, r);
            
        }
    }
    
    return destination;
}

BriskImage *briskLayerTwoThirdSample(const BriskImage *source,
                                     BriskImage *destination)
{
    /* 3/2 resampling, thus scale = 1.5 */
    const b_float_t scale = 1.5f;
    b_int_t i, j;
    
    assert((source->width / 3) * 2 == destination->width);
    assert((source->height / 3) * 2 == destination->height);
    
    for(i = 0; i < destination->width; i++)
    {
        for(j = 0; j < destination->height; j++)
        {
            b_int_t x = i * scale;
            b_int_t y = j * scale;
            
            b_float_t u = 0.1f; /*(float)i * scale - x; */
            b_float_t v = 0.1f; /*(float)j * scale - y; */
            
            b_uint8_t r1 = _grayGetPixel(source, x, y);
            b_uint8_t r2 = _grayGetPixel(source, x, y + 1);
            b_uint8_t r3 = _grayGetPixel(source, x + 1, y);
            b_uint8_t r4 = _grayGetPixel(source, x + 1, y + 1);
            
            b_uint8_t r = (b_uint8_t)((1.0f - u) * (1.0f - v) * r1 + (1.0f - u) * v
                                  * r2 + u * (1.0f - v) * r3 + u * v * r4);
            
            _graySetPixel(destination, i, j, r);
        }
    }
    
    return destination;
}
