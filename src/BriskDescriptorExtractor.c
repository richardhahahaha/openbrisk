/*
 * Copyright 2013 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <BRISK/include/BriskPrototypes.h>

#define BASIC_SIZE   12.0f
#define SCALES       64
/* 40->4 Octaves - else, this needs to be adjusted... */
#define SCALE_RANGE  30
/* discretization of the rotation look-up */
#define N_ROT        32 /* 1024 */

#ifndef M_PI
#   define M_PI 3.14159265358979323846264338327950288
#endif

static void generateKernel(struct BriskDescriptorExtractor *extractor,
                           FloatVector *radiusList,
                           IntVector *numberList,
                           b_float_t distanceMax, b_float_t distanceMin);

struct BriskDescriptorExtractor *
briskDescriptorExtractorinit(struct BriskDescriptorExtractor *extractor,
                             b_float_t patternScale)
{
    IntVector   numberList;
    FloatVector radiusList;
    
    const b_float_t scale085 = 0.85f * patternScale;
    
    extractor->rotationInvariance = true;
    extractor->scaleInvariance    = true;
    
    SV_INIT(&numberList, 5, b_int_t);
    SV_INIT(&radiusList, 5, b_float_t);
    
    SV_PUSH_BACK(&radiusList, scale085 * 0.0f,  b_float_t);
    SV_PUSH_BACK(&radiusList, scale085 * 2.9f,  b_float_t);
    SV_PUSH_BACK(&radiusList, scale085 * 4.9f,  b_float_t);
    SV_PUSH_BACK(&radiusList, scale085 * 7.4f,  b_float_t);
    SV_PUSH_BACK(&radiusList, scale085 * 10.8f, b_float_t);
    
    SV_PUSH_BACK(&numberList, 1,  b_int_t);
    SV_PUSH_BACK(&numberList, 10, b_int_t);
    SV_PUSH_BACK(&numberList, 14, b_int_t);
    SV_PUSH_BACK(&numberList, 15, b_int_t);
    SV_PUSH_BACK(&numberList, 20, b_int_t);
    
    generateKernel(extractor, &radiusList, &numberList,
                   5.85f * patternScale,
                   8.2f * patternScale);
    
    BRISK_MFREE(radiusList.data);
    BRISK_MFREE(numberList.data);
    
    radiusList.data = NULL;
    numberList.data = NULL;

    return extractor;
}

struct BriskDescriptorExtractor *
briskDescriptorExtractorCreate(b_float_t patternScale)
{
    struct BriskDescriptorExtractor *extractor =
        (struct BriskDescriptorExtractor *)
            BRISK_MALLOC(sizeof(struct BriskDescriptorExtractor));
    BRISK_MALLOC_FAIL_CHECK(extractor);
    
    briskDescriptorExtractorinit(extractor, patternScale);
    
    return extractor;
}

void briskDescriptorExtractorDestroy(struct BriskDescriptorExtractor *extractor)
{
    if(extractor)
    {
        BRISK_MFREE(extractor->patternPoints); extractor->patternPoints = NULL;
        BRISK_MFREE(extractor->shortPairs);    extractor->shortPairs = NULL;
        BRISK_MFREE(extractor->longPairs);     extractor->longPairs = NULL;
        BRISK_MFREE(extractor->scaleList);     extractor->scaleList = NULL;
        BRISK_MFREE(extractor->sizeList);      extractor->sizeList = NULL;
        BRISK_MFREE(extractor);                extractor = NULL;
    }
}

static inline bool roiPredicate(const b_float_t minX, const b_float_t minY,
                                const b_float_t maxX, const b_float_t maxY,
                                const FeaturePoint keyPt)
{
    return (keyPt.x < minX) || (keyPt.x >= maxX) ||
           (keyPt.y < minY) || (keyPt.y >= maxY);
}

void generateKernel(struct BriskDescriptorExtractor *extractor,
                    FloatVector *radiusList,
                    IntVector *numberList,
                    b_float_t distanceMax, b_float_t distanceMin)
{
    struct BriskPatternPoint *patternIterator;
    
    b_int_t ring, k, *idxChange;
    b_size_t i, j, rot, scale, indSize;
    b_float_t alpha, theta, sinNumberListBuffer[SV_SIZE(numberList)];
    
    const b_int_t rings = (b_int_t)SV_SIZE(radiusList);
    const b_float_t sigmaScale = 1.3f;
    const b_float_t lbScaleStep = 0.076670162380f;

    extractor->distanceMax = distanceMax;
    extractor->distanceMin = distanceMin;
    extractor->points = 0;
    
    for(ring = 0; ring < rings; ring++)
        extractor->points += SV_AT(numberList, ring);
    
    assert(extractor->points);
    
    /* set up the patterns */
    extractor->patternPoints = (struct BriskPatternPoint *)
        BRISK_MALLOC(sizeof(struct BriskPatternPoint) *
           extractor->points * SCALES * N_ROT);
    BRISK_MALLOC_FAIL_CHECK(extractor->patternPoints);

    patternIterator = extractor->patternPoints;
    
    extractor->scaleList =
        (b_float_t *)BRISK_MALLOC(sizeof(b_float_t) * SCALES);
    BRISK_MALLOC_FAIL_CHECK(extractor->scaleList);
    
    extractor->sizeList =
        (b_uint32_t *)BRISK_MALLOC(sizeof(b_uint32_t) * SCALES);
    BRISK_MALLOC_FAIL_CHECK(extractor->sizeList);
    
    for(i = 0; i < SV_SIZE(numberList); i++)
        sinNumberListBuffer[i] = sinf(SV_AT(numberList, i));
    
    for(scale = 0; scale < SCALES; scale++)
    {
        extractor->scaleList[scale] =
            pow(2.0f, (b_float_t)(scale * lbScaleStep));
        extractor->sizeList[scale] = 0;
        
        /* generate the pattern points look-up */
        for(rot = 0; rot < N_ROT; rot++)
        {
            theta = rot * 2.0f * M_PI / (b_float_t)N_ROT;
            for(ring = 0; ring < rings; ring++)
            {
                for(k = 0; k < SV_AT(numberList, ring); k++)
                {
                    b_uint32_t size;
                    
                    alpha = k * 2.0f * M_PI /
                            (b_float_t)(SV_AT(numberList, ring));

                    patternIterator->x = extractor->scaleList[scale]
                                         * SV_AT(radiusList, ring)
                                         * cosf(alpha + theta);
                    
                    patternIterator->y = extractor->scaleList[scale]
                                         * SV_AT(radiusList, ring)
                                         * sinf(alpha + theta);
                    
                    if(ring == 0)
                        patternIterator->sigma = sigmaScale *
                                          extractor->scaleList[scale] * 0.5f;
                    else
                    {
                        patternIterator->sigma = sigmaScale *
                        extractor->scaleList[scale]
                        * ((b_float_t)(SV_AT(radiusList, ring)))
                        * sinNumberListBuffer[ring];
                    }

                    size =
                        ceilf(((extractor->scaleList[scale]
                        * SV_AT(radiusList, ring))
                        + patternIterator->sigma)) + 1.0f;
                    
                    if(extractor->sizeList[scale] < size)
                        extractor->sizeList[scale] = size;
                    
                    patternIterator++;
                }
            }
        }
    }
    
    extractor->shortPairs = (struct BriskShortPair *)
        BRISK_MALLOC(sizeof(struct BriskShortPair)
               * extractor->points * (extractor->points -1) / 2);
    BRISK_MALLOC_FAIL_CHECK(extractor->shortPairs);
    
    extractor->longPairs = (struct BriskLongPair *)
        BRISK_MALLOC(sizeof(struct BriskLongPair) * extractor->points
               * (extractor->points - 1) / 2);
    BRISK_MALLOC_FAIL_CHECK(extractor->longPairs);
    
    extractor->noShortPairs = 0;
    extractor->noLongPairs = 0;
    
    indSize = extractor->points * (extractor->points -1) / 2;
    idxChange = (b_int_t *)BRISK_MALLOC(sizeof(b_int_t) * indSize);
    
    for(i = 0; i < indSize; i++)
        idxChange[i] = (b_int_t)i;

    const b_float_t distanceMinSq = extractor->distanceMin * extractor->distanceMin;
    const b_float_t distanceMaxSq = extractor->distanceMax * extractor->distanceMax;
    
    for(i = 1; i < extractor->points; i++)
    {
        for(j = 0; j < i; j++)
        {
            const b_float_t dx = extractor->patternPoints[j].x
                                 - extractor->patternPoints[i].x;
            const b_float_t dy = extractor->patternPoints[j].y
                                 - extractor->patternPoints[i].y;
            const b_float_t normSq = (dx * dx + dy * dy);
            
            if(normSq > distanceMinSq)
            {
                /* save to long pairs */
                struct BriskLongPair *longPair =
                    &extractor->longPairs[extractor->noLongPairs];
                
                longPair->weightedDx = (b_int_t)((dx / (normSq)) * 2048.0f + 0.5f);
                longPair->weightedDy = (b_int_t)((dy / (normSq)) * 2048.0f + 0.5f);
                longPair->i = i;
                longPair->j = j;
                extractor->noLongPairs++;
            }
            else if (normSq < distanceMaxSq)
            {
                struct BriskShortPair *shortPair =
                    &extractor->shortPairs[idxChange[extractor->noShortPairs]];
                
                /* save to short pairs */
                assert(extractor->noShortPairs < indSize);
            
                shortPair->j = j;
                shortPair->i = i;
                extractor->noShortPairs++;
            }
        }
    }
    
    extractor->strings = (b_int_t)ceil(((b_float_t)(extractor->noShortPairs))
                          / 128.0f) * 4 * 4;

    BRISK_MFREE(idxChange);
    idxChange = NULL;
}

static b_int_t smoothedIntensity(struct BriskDescriptorExtractor *extractor,
                                 const BriskImage* image, const b_float_t keyX,
                                 const b_float_t keyY, const b_uint_t scale,
                                 const b_uint_t rot, const b_uint_t point,
                                 b_uint32_t *integral)
{
    const struct BriskPatternPoint briskPoint =
        extractor->patternPoints[scale * N_ROT * extractor->points
                                 + rot * extractor->points + point];
    
    const b_float_t xf = briskPoint.x + keyX;
    const b_float_t yf = briskPoint.y + keyY;
    const b_int_t x = (b_int_t)xf;
    const b_int_t y = (b_int_t)yf;
    const b_int_t imageCols=image->width;
    
    const b_float_t sigmaHalf = briskPoint.sigma;
    const b_float_t area = 4.0f * sigmaHalf * sigmaHalf;
    
    const b_int_t scaling  = 4194304.0f / area;
    const b_int_t scaling2 = (b_float_t)(scaling) * area / 1024.0f;
    
    const b_int_t integralcols = imageCols + 1;
    
    const b_float_t x_1 = xf - sigmaHalf;
    const b_float_t x1  = xf + sigmaHalf;
    const b_float_t y_1 = yf - sigmaHalf;
    const b_float_t y1  = yf + sigmaHalf;
    
    const b_int_t xLeft   = (b_int_t)(x_1 + 0.5f);
    const b_int_t yTop    = (b_int_t)(y_1 + 0.5f);
    const b_int_t xRight  = (b_int_t)(x1  + 0.5f);
    const b_int_t yBottom = (b_int_t)(y1  + 0.5f);
    
    const b_float_t rx_1 = (b_float_t)(xLeft) - x_1  + 0.5f;
    const b_float_t ry_1 = (b_float_t)(yTop) - y_1   + 0.5f;
    const b_float_t rx1  = x1 - (b_float_t)(xRight)  + 0.5f;
    const b_float_t ry1  = y1 - (b_float_t)(yBottom) + 0.5f;
    
    const b_int_t dx = xRight - xLeft - 1;
    const b_int_t dy = yBottom - yTop - 1;
    
    const b_int_t A = (rx_1 * ry_1) * scaling;
    const b_int_t B = (rx1  * ry_1) * scaling;
    const b_int_t C = (rx1  * ry1)  * scaling;
    const b_int_t D = (rx_1 * ry1)  * scaling;
    
    const b_int_t rx_1i = rx_1 * scaling;
    const b_int_t ry_1i = ry_1 * scaling;
    
    const b_int_t rx1i = rx1 * scaling;
    const b_int_t ry1i = ry1 * scaling;
    
    b_int_t result;
    b_uint8_t *ptr, *end1, *end_j, *end3;
    
    if(sigmaHalf < 0.5f)
    {
        const b_int_t rx = (xf - x) * 1024;
        const b_int_t ry = (yf - y) * 1024;
        const b_int_t rx1 = (1024 - rx);
        const b_int_t ry1 = (1024 - ry);
        
        ptr = image->pixel + x + y * imageCols;

        result =  (rx1 * ry1 * (b_int_t)*ptr);   ptr++;
        result += (rx  * ry1 * (b_int_t)*ptr);   ptr += imageCols;
        result += (rx  * ry  * (b_int_t)*ptr);   ptr--;
        result += (rx1 * ry  * (b_int_t)*ptr);
        
        return (result + 512) / 1024;
    }
    
    if(dx + dy > 2)
    {
        b_uint8_t *ptr = image->pixel + xLeft + imageCols * yTop;
        b_uint32_t *ptrIntegral = integral + xLeft + integralcols * yTop + 1;
        
        /* find a simple path through the different surface corners */
        const b_int_t tmp1  = *ptrIntegral;  ptrIntegral += dx;
        const b_int_t tmp2  = *ptrIntegral;  ptrIntegral += integralcols;
        const b_int_t tmp3  = *ptrIntegral;  ptrIntegral += 1;
        const b_int_t tmp4  = *ptrIntegral;  ptrIntegral += dy * integralcols;
        const b_int_t tmp5  = *ptrIntegral;  ptrIntegral -= 1;
        const b_int_t tmp6  = *ptrIntegral;  ptrIntegral += integralcols;
        const b_int_t tmp7  = *ptrIntegral;  ptrIntegral -= dx;
        const b_int_t tmp8  = *ptrIntegral;  ptrIntegral -= integralcols;
        const b_int_t tmp9  = *ptrIntegral;  ptrIntegral -= 1;
        const b_int_t tmp10 = *ptrIntegral;  ptrIntegral -= dy *integralcols;
        const b_int_t tmp11 = *ptrIntegral;  ptrIntegral += 1;
        const b_int_t tmp12 = *ptrIntegral;
        
        /* assign the weighted surface integrals */
        const b_int_t upper  = (tmp3 - tmp2  + tmp1  - tmp12) * ry_1i;
        const b_int_t middle = (tmp6 - tmp3  + tmp12 - tmp9 ) * scaling;
        const b_int_t left   = (tmp9 - tmp12 + tmp11 - tmp10) * rx_1i;
        const b_int_t right  = (tmp5 - tmp4  + tmp3  - tmp6 ) * rx1i;
        const b_int_t bottom = (tmp7 - tmp6  + tmp9  - tmp8 ) * ry1i;
        
        result =  A * (b_int_t)*ptr;   ptr += dx + 1;
        result += B * (b_int_t)*ptr;   ptr += dy * imageCols + 1;
        result += C * (b_int_t)*ptr;   ptr -= dx + 1;
        result += D * (b_int_t)*ptr;
        
        return (result + upper + middle + left + right + bottom + scaling2 / 2)
                / scaling2;
    }
    
    ptr = image->pixel + xLeft + imageCols * yTop;

    result = A * (b_int_t)*ptr;
    ptr++;
    
    end1 = ptr + dx;
    
    for(; ptr < end1; ptr++)
        result += ry_1i * (b_int_t)*ptr;

    result += B * (b_int_t)(*ptr);
    
    /* middle ones */
    ptr += imageCols - dx - 1;
    end_j = ptr + dy * imageCols;
    for(; ptr < end_j; ptr += imageCols - dx - 1)
    {
        b_uint8_t *end2;
        
        result += rx_1i * (b_int_t)*ptr;
        ptr++;
        end2 = ptr + dx;
        
        for(; ptr < end2; ptr++)
            result += (b_int_t)*ptr * scaling;
        
        result += rx1i * (b_int_t)*ptr;
    }
    
    /* last row */
    result += D * (b_int_t)(*ptr);
    ptr++;
    end3 = ptr + dx;
    
    for(; ptr < end3; ptr++)
        result += ry1i * (b_int_t)*ptr;
    
    result += C * (b_int_t)*ptr;
    
    return (result + scaling2 / 2) / scaling2;
}

#define __MAX_(__x, __y) ((__x) < (__y) ? (__y) : (__x))

FeaturePointContainer *briskExtractFeatures(
    struct BriskDescriptorExtractor *extractor,
    const BriskImage *image,
    FeaturePointContainer *featurePoints)
{
    b_int_t kSize = (b_int_t)SV_SIZE(featurePoints);
    b_int_t width, height, x, y;
    b_int_t *values, t1, t2, dir0, dir1;
    
    IntVector kScales;
    
    const b_float_t log2 = 0.693147180559945f;
    const b_float_t lb_scalerange = log(SCALE_RANGE) / (log2);
    const b_float_t basicSize06 = BASIC_SIZE * 0.6f;
    b_uint_t basicScale = 0, i;
    b_size_t k;
    
    if(!extractor->scaleInvariance)
    {
        basicScale = __MAX_((b_int_t)(SCALES / lb_scalerange *
                            (log(1.45f * BASIC_SIZE
                            / (basicSize06)) / log2) + 0.5f), 0);
    }
    
    SV_INIT(&kScales, kSize, b_int_t);

    for(k = 0; k < kSize; k++)
    {
        b_uint_t scale;
        b_int_t border, borderX, borderY;
        
        if(extractor->scaleInvariance)
        {
            scale = __MAX_((b_int_t)(SCALES / lb_scalerange
                    * (log(featurePoints->data[k].size
                           / (basicSize06)) / log2) + 0.5f), 0);

            if(scale >= SCALES)
                scale = SCALES - 1;
            
            SV_PUSH_BACK(&kScales, scale, b_int_t);
        }
        else
        {
            scale = basicScale;
            SV_PUSH_BACK(&kScales, scale, b_int_t);
        }
        
        border  = extractor->sizeList[scale];
        borderX = image->width  - border;
        borderY = image->height - border;
        
        if(roiPredicate(border, border, borderX, borderY,
                        SV_AT(featurePoints, k)))
        {
            SIMPLE_VECTOR_ERASE_AT(featurePoints, k, FeaturePoint);
            SIMPLE_VECTOR_ERASE_AT(&kScales, k, b_int_t);

            kSize--;  k--;
        }
    }
    
    /* calculate the integral image */
    b_uint32_t *integration;
    integration = (b_uint32_t *)BRISK_MALLOC(sizeof(b_uint32_t)
                                             * (image->width + 1)
                                             * (image->height + 1));
    BRISK_MALLOC_FAIL_CHECK(integration);
    
    width = image->width + 1;
    height = image->height + 1;
    
    for(x = 0; x < width; x++)
        integration[x] = 0;
    
    for(y = 0; y < height; y++)
        integration[y * width] = 0;
    
    for(x = 1; x < width; x++)
    {
        for(y = 1; y < height; y++)
        {
            integration[y * width + x] =
                image->pixel[image->width * (y - 1) + (x - 1)]
                + integration[y * width + (x - 1)]
                + integration[(y - 1) * width + x]
                - integration[(y - 1) * width + (x - 1)];
        }
    }
        
    values = (b_int_t *)BRISK_MALLOC(sizeof(b_int_t) * extractor->points);

    for(k = 0; k < kSize; k++)
    {
        FeaturePoint *featurePoint = &SV_AT(featurePoints, k);
        
        b_int_t theta, shifter = 0;
        b_int_t *ptrValues = values;
        
        const b_int_t scale = SV_AT(&kScales, k);
        const b_float_t x = featurePoint->x;
        const b_float_t y = featurePoint->y;
        
        if (!extractor->rotationInvariance)
            theta = 0;
        else
        {
            /* get the gray values in the unrotated pattern */
            for(i = 0; i < extractor->points; i++)
                *(ptrValues++) = smoothedIntensity(extractor, image, x,
                                                 y, scale, 0, i,integration);
            dir0 = 0;
            dir1 = 0;
            
            /* now iterate through the long pairings */
            const struct BriskLongPair *max = extractor->longPairs +
                                              extractor->noLongPairs;
            struct BriskLongPair *iter;
            
            for(iter = extractor->longPairs;
                iter < max; iter++)
            {
                t1 = *(values + iter->i);
                t2 = *(values + iter->j);
                
                const b_int_t delta_t = t1 - t2;
                const b_int_t tmp0 = delta_t * (iter->weightedDx) / 1024;
                const b_int_t tmp1 = delta_t * (iter->weightedDy) / 1024;
                
                dir0 += tmp0;
                dir1 += tmp1;
            }
            
            featurePoint->angle = atan2f(dir1, dir0) / M_PI * 180.0f;
            
            theta = (b_int_t)((N_ROT * featurePoint->angle) / 360.0 + 0.5);
            
            if(theta < 0)
                theta += N_ROT;
            if(theta >= N_ROT)
                theta -= N_ROT;
        }
        
        shifter = 0;
        ptrValues = values;

        for(i = 0; i < extractor->points; i++)
            *(ptrValues++) = smoothedIntensity(extractor, image, x, y,
                                               scale, theta, i,integration);
        
        /* descriptor length: 16 * 32-bit (512-bit) */
        featurePoint->descriptor = BRISK_MALLOC(sizeof(b_uint32_t) * 16);
        BRISK_MALLOC_FAIL_CHECK(featurePoint->descriptor);
        /* clear the memory block */
        BRISK_MEMSET(featurePoint->descriptor, 0, sizeof(b_uint32_t) * 16);
        
        b_uint32_t *ptr = featurePoint->descriptor;
        
        const struct BriskShortPair *max =
            extractor->shortPairs + extractor->noShortPairs;

        struct BriskShortPair *iter;
        
        for(iter = extractor->shortPairs;
            iter < max; iter++)
        {
            t1 = *(values + iter->i);
            t2 = *(values + iter->j);
            if(t1 > t2)
                *ptr |= (1 << shifter);

            shifter++;

            if(shifter == 32)
            {
                shifter = 0;
                ptr++;
            }
        }
    }
    
    /* cleanup */
    BRISK_MFREE(values);
    BRISK_MFREE(integration);
    BRISK_MFREE(kScales.data);
    
    integration  = NULL;
    values       = NULL;
    kScales.data = NULL;
    
    return featurePoints;
}

#undef __MAX_
