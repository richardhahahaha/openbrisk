/*
 * Copyright 2013 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <BRISK/include/BriskPrototypes.h>

FeaturePoint featurePointMake(b_float_t x, b_float_t y, b_float_t size,
                              b_float_t angle, b_float_t response,
                              b_int_t octave)
{
    FeaturePoint featurePoint;
    featurePoint.x = x;
    featurePoint.y = y;
    featurePoint.size = size;
    featurePoint.angle = angle;
    featurePoint.response = response;
    featurePoint.octave = octave;
    
    featurePoint.descriptor = NULL;
    
    return featurePoint;
}

void featurePointDestroy(struct FeaturePoint *featurePoint)
{
    BRISK_MFREE(featurePoint->descriptor);
}

void featurePointsDestroy(FeaturePointContainer *featurePoints)
{
    register b_size_t i;
    for(i = 0; i < SIMPLE_VECTOR_SIZE(featurePoints); i++)
        featurePointDestroy(&SIMPLE_VECTOR_AT(featurePoints, i));
    
    SIMPLE_VECTOR_RELEASE(featurePoints);
}
