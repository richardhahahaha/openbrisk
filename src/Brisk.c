/*
 * Copyright 2013 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>

#include <BRISK/include/Brisk.h>

FeaturePointContainer *brisk(const BriskImage *image,
                             const b_uint16_t threshold,
                             const b_uint8_t octaves,
                             const b_float_t extractorPatternScale)
{
    struct BriskDescriptorExtractor *extractor;
    struct BriskScaleSpace *briskScaleSpace;
    FeaturePointContainer *featuresPoints;
    
    /* for b_uint32_t, pow(2, 32) / 255 ~= 16843009. */
    if(image->width * image->height > 16843009)
    {
        fprintf(stderr, "[Warning]: image too large to use 32-bit "
                        "unsigned integer for integration image. ");
    }
    
    /* create the extractor */
    extractor = briskDescriptorExtractorCreate(extractorPatternScale);
    
    /* construct the scale space and its image pyramid */
    briskScaleSpace =
        briskConstructPyramid(briskScaleSpaceCreate(octaves), image);
    
    /* detect feature points */
    featuresPoints = briskDetectFeaturePoints(briskScaleSpace, threshold);
    
    /* calculate descriptors */
    featuresPoints = briskExtractFeatures(extractor, image, featuresPoints);
    
    /* clean up */
    briskScaleSpaceDestroy(briskScaleSpace);
    briskDescriptorExtractorDestroy(extractor);
    
    return featuresPoints;
}
